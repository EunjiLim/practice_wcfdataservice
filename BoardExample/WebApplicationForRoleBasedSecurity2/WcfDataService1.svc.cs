//------------------------------------------------------------------------------
// <copyright file="WebDataService.svc.cs" company="Microsoft">
//     Copyright (c) Microsoft Corporation.  All rights reserved.
// </copyright>
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Data.Services;
using System.Data.Services.Common;
using System.Linq;
using System.ServiceModel.Web;
using System.Web;

namespace WebApplicationForRoleBasedSecurity2
{
    public class WcfDataService1 : DataService<Board2Entities>
    {
        // This method is called only once to initialize service-wide policies.
        public static void InitializeService(DataServiceConfiguration config)
        {
            // TODO: set rules to indicate which entity sets and service operations are visible, updatable, etc.
            // Examples:
             config.SetEntitySetAccessRule("*", EntitySetRights.AllRead);
            // config.SetServiceOperationAccessRule("MyServiceOperation", ServiceOperationRights.All);
            config.DataServiceBehavior.MaxProtocolVersion = DataServiceProtocolVersion.V3;
        }
    }

    public class RoleBasedAuthModule : IHttpModule
    {
        public void Init(HttpApplication context)
        {
            context.AuthenticateRequest += new
                EventHandler(OnAuthenticateRequest);
        }

        void OnAuthenticateRequest(object sender, EventArgs e)
        {
            var app = sender as HttpApplication;
            var request = app.Context.Request;
            var userId = request.Headers["odata-user-id"];

            if (string.IsNullOrEmpty(userId))
            {
                userId = "Anonymous";
            }

            var roles = GetRolesForUser(userId);

            var gPrincipal 
        }
    }
}
