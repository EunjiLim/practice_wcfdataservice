﻿using System;
using System.Collections.Generic;
using System.Data.Services.Client;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConsoleApplicationForRoleBasedSecurity.BoardServiceReference;

namespace ConsoleApplicationForRoleBasedSecurity
{
    class Program
    {
        static void Main(string[] args)
        {
            InvokeODataSeprviceOeration();
        }

        static void InvokeODataSeprviceOeration()
        {
            var svcUri = new Uri("http://localhost:57615/WcfBoardService.svc");
            var ctx = new Board2Entities(svcUri);

            ctx.SendingRequest2 += (sender, args) =>
            {
                args.RequestMessage.SetHeader("odata-user-id", "Admin");
            };

            try
            {
                var articles = ctx.Articles.ToList();
                Console.WriteLine("There are {0} articles.", articles.Count);
                Console.Read();
            }
            catch (DataServiceQueryException e)
            {
                Debug.WriteLine(e.ToString());
            }

        }
    }
}
