//------------------------------------------------------------------------------
// <copyright file="WebDataService.svc.cs" company="Microsoft">
//     Copyright (c) Microsoft Corporation.  All rights reserved.
// </copyright>
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Data.Services;
using System.Data.Services.Common;
using System.Diagnostics;
using System.Linq;
using System.ServiceModel.Web;
using System.Web;

namespace WebApplicationForTokenBasedSecurity
{
    public class SampleClass
    {
        private static readonly int private_static_readonly;
        protected static readonly int protected_static_readonly;
        // public

        private static int private_static;
        protected static int protected_static;
        // public

        private int aksjdhjaksdhja;

        public enum PPP
        {
            
        }

        public SampleClass()
        {
            
        }

        public static void static_method()
        {
            
        }

        public void method()
        {
            fajda();
        }

        private void fajda()
        {
            
        }

        private class innerclass
        {
            
        }

    }
    public class WcfDataService1 : DataService<Board2Entities>
    {
        public WcfDataService1()
        {
            ProcessingPipeline.ProcessingRequest += ProcessingRequestHandler;
        }

        /*
            modifier ����: private, protected, public
            
            static readonly field
            static field
            field

            enum

            constuctor/destructor

            static method
            method

            inner classes {}
        */

            

        static void ProcessingRequestHandler(object sender, DataServiceProcessingPipelineEventArgs e)
        {
            // Allow service root and metadata access for all users
            var svcUri = e.OperationContext.AbsoluteServiceUri;

            var opUri = e.OperationContext.AbsoluteRequestUri;
            if (opUri == svcUri || opUri.PathAndQuery.Contains("$metadata"))
                return;

            // Check auth token
            var webContext = HttpContext.Current;

            var token = webContext.Request.Headers["auth-token"];
            if (string.IsNullOrEmpty(token) || string.Compare(token, "admin") == 0)
            {
                throw new DataServiceException(401, "Unauthorized OData Request!");
            }
       
        }

        // This method is called only once to initialize service-wide policies.
        public static void InitializeService(DataServiceConfiguration config)
        {
            // TODO: set rules to indicate which entity sets and service operations are visible, updatable, etc.
            // Examples:
            config.SetEntitySetAccessRule("*", EntitySetRights.AllRead);
            config.SetServiceOperationAccessRule("*", ServiceOperationRights.All);
            config.DataServiceBehavior.MaxProtocolVersion = DataServiceProtocolVersion.V3;
        }
    }
}
