﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using ConsoleApplicationForASPdotNetFormsAuth.ServiceReference1;

namespace ConsoleApplicationForASPdotNetFormsAuth
{
    class Program
    {
        static void Main(string[] args)
        {
            AccessODataService();
        }

        static string GetAuthenticationCookie(string username, string password)
        {
            var request = (HttpWebRequest)WebRequest.Create("http://hohohoho.azurewebsites.net/ODataFormsAuthHandler.ashx");
            request.Method = "POST";
            request.ContentType = "application/x-www-form-urlencoded";
            using (var sw = new StreamWriter(request.GetRequestStream()))
            {
                sw.Write("username=" + username + "&password=" + password);
            }
            var response = (HttpWebResponse)request.GetResponse();
            return response.Headers[HttpResponseHeader.SetCookie];
        }

        static void AccessODataService()
        {
            var svcUri = new
            Uri("http://hohohoho.azurewebsites.net/WcfDataService1.svc");
            var ctx = new Board2Entities(svcUri);
            // Injecting the cookie for forms authentication token
            ctx.SendingRequest += (o, e) =>
            {
                var cookie = GetAuthenticationCookie("user1", "password");
                e.RequestHeaders.Add(HttpRequestHeader.Cookie, cookie);
            };
            var users = ctx.Users.ToList();
            Console.WriteLine("There are {0} categories.", users.Count);
            Console.ReadLine();
        }
    }
}
