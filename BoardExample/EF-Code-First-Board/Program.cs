﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace EF_Code_First_Board
{
    public class BoardContext : DbContext
    {
        public BoardContext() : base()
        {
            
        }

        public DbSet<User> Users { get; set; }
        public DbSet<Article> Articles { get; set; }
        public DbSet<Comment> Comments { get; set; }
        public DbSet<Notification> Notifications { get; set; }
    }

    class Program
    {
        static void Main(string[] args)
        {
            DbConfiguration.SetConfiguration(new MySql.Data.Entity.MySqlEFConfiguration());
            Database.SetInitializer(new DropCreateDatabaseIfModelChanges<BoardContext>());

            using (var ctx = new BoardContext())                              
            {
                User user1 = new User {Name = "user1"};
                //User user2 = new User {Name = "user2"};
                //User user3 = new User {Name = "user3"};

                /*Article article1 = new Article
                {
                    User = user1,
                    Title = "First Article by user1",
                    Content = "First Article by user1"
                };
                Article article2 = new Article
                {
                    User = user2,
                    Title = "First Article by user2",
                    Content = "First Article by user2"
                };*/

                ctx.Users.Add(user1);
                //ctx.Users.Add(user2);
                //ctx.Users.Add(user3);
                ctx.SaveChanges();
            }
        }
    }

    public class User
    {
        public User()
        {
            Articles = new List<Article>();
            Notifications = new List<Notification>();
        }
     
        [Key]
        [Column("id")]
        public int Id { get; set; }

        [Column("name")]
        public string Name { get; set; }

        public ICollection<Article> Articles { get; set; }
        public ICollection<Notification> Notifications { get; set; }  
        public ICollection<Comment> Comments { get; set; }
    }

    public class Article
    {
        [Key]
        [Column("id")]
        public int Id { get; set; }

        [Column("user_id")]
        public int UserId { get; set; }

        [Column("title")]
        public string Title { get; set; }

        [Column("content")]
        public string Content { get; set; }

        [ForeignKey("UserId")]
        public User User { get; set; }

        public ICollection<Comment> Comments { get; set; }

        public void WriteComment(Comment comment)
        {
            NotifyToRelatedAuthors(this, Notification.CreateForCreatingComment(comment, DateTime.Now));
            Comments.Add(comment);
        }

        private void NotifyToRelatedAuthors(Article article, Notification notification)
        {
            NotifyToAuthor(article.User, notification);

            foreach (var c in article.Comments)
                NotifyToAuthor(c.User, notification);
        }

        private void NotifyToAuthor(User user, Notification notification)
        {
            //Notification 테이블에 추가됨.????????
            user.Notifications.Add(notification);
        }
    }

    public class Comment
    {
        [Key]
        [Column("id")]
        public int Id { get; set; }

        [Column("user_id")]
        public int UserId { get; set; }

        [Column("content")]
        public string Content { get; set; }

        [Column("article_id")]
        public int ArticleId { get; set; }

        [ForeignKey("ArticleId")]
        public Article Article { get; set; }

        [ForeignKey("UserId")]
        public User User { get; set; }
    }

    public class Notification
    {
        [Key]
        [Column("id")]
        public int Id { get; set; }

        [Column("user_id")]
        public int UserId { get; set; }

        [Column("message")]
        public string Message { get; set; }

        [Column("notified_datetime")]
        public DateTime NotifiedDateTime { get; set; }

        [ForeignKey("UserId")]
        public User User { get; set; }

        public static Notification CreateForCreatingComment(Comment comment, DateTime notifiedDate)
        {
            return new Notification
            {
                Message = comment.User.Name + "wrote a comment on Article" + comment.Article.Id,
                NotifiedDateTime = notifiedDate
            };
        }
    }
}
