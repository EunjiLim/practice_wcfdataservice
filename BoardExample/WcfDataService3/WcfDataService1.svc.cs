//------------------------------------------------------------------------------
// <copyright file="WebDataService.svc.cs" company="Microsoft">
//     Copyright (c) Microsoft Corporation.  All rights reserved.
// </copyright>
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Services;
using System.Data.Services.Client;
using System.Data.Services.Common;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Runtime.InteropServices.ComTypes;
using System.Runtime.Serialization.Json;
using System.Security.Cryptography.X509Certificates;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Web;
using System.Web.Security;

namespace WcfDataService3
{
    public class WcfDataService1 : DataService<Board2Entities>
    {
        // This method is called only once to initialize service-wide policies.
        public static void InitializeService(DataServiceConfiguration config)
        {
            // TODO: set rules to indicate which entity sets and service operations are visible, updatable, etc.
            // Examples:
            // config.SetEntitySetAccessRule("MyEntityset", EntitySetRights.AllRead);
            // config.SetServiceOperationAccessRule("MyServiceOperation", ServiceOperationRights.All);

            try
            {
              config.UseVerboseErrors = true;
            config.SetEntitySetAccessRule("*", EntitySetRights.All);
            //config.SetServiceOperationAccessRule("*", ServiceOperationRights.All);
            config.DataServiceBehavior.MaxProtocolVersion = DataServiceProtocolVersion.V3;
            }
            catch (Exception)
            {
              
                throw new DataServiceException("-_-");
            }


     
        }

        // Change Interceptor for Comment entity set
        /*[ChangeInterceptor("Comments")]
        public void OnChangeComments(Comments comment, UpdateOperations operations)
        {
            if (operations == UpdateOperations.Add)
            {
                var t = DateTime.Now;

                if (comment.user_id != comment.Articles.Users.id)
                    comment.Articles.Users.Notifications.Add(Notifications.CreateForCreatingComment(comment.Articles.Users, comment, t));

                //comment.Articles.Users != c.User일 때 Union해야 하는데.............
                var users = (from c in comment.Articles.Comments
                    where c.user_id != comment.user_id && c.user_id != comment.Articles.Users.id
                    select c.Users).Distinct();

                foreach (var u in users)
                    u.Notifications.Add(Notifications.CreateForCreatingComment(u, comment, t));
            }
            // try {
            // var comment = new Comment() { };
            // interceptor(comment);
            // context.Comments.Add(comment);
            // context.SaveChanges();
            // } catch (...) { }
            // db -> entity

        }*/

        /*[WebGet]
        public DateTime GetServerTime()
        {
            return DateTime.Now;
        }

        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json)]
        public string CustomMethod(String myParameter)
        {
            string body =
                Encoding.UTF8.GetString(OperationContext.Current.RequestContext.RequestMessage.GetBody<byte[]>());

            var u = new Users() {name = "dd"};
            var e = new Board2Entities();
            e.Users.Add(u);
            e.SaveChanges();

            return "yes" + body;
        }

        [WebInvoke(Method = "POST", UriTemplate = "{param0}/{param1}", RequestFormat = WebMessageFormat.Json)]
        public int AddUser(string param0)
        {
            string body =
                Encoding.UTF8.GetString(OperationContext.Current.RequestContext.RequestMessage.GetBody<byte[]>());
            var user = new Users() {name = body};
            var ctx = new Board2Entities();
            ctx.Users.Add(user);
            ctx.SaveChanges();
            return 1;
        }

        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json)]
        public int UpdateUser()
        { 
            string body =
                Encoding.UTF8.GetString(OperationContext.Current.RequestContext.RequestMessage.GetBody<byte[]>());

            var ctx = new Board2Entities();
            DataContractJsonSerializer jsonSerializer =
                new DataContractJsonSerializer(typeof(Users));
            MemoryStream ms = new MemoryStream(Encoding.ASCII.GetBytes(body));

            Users user = (Users)jsonSerializer.ReadObject(ms);
            ctx.Users.Local[user.id].name = user.name;
            ctx.SaveChanges();
            ms.Close();

            return 1;
        }

        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json)]
        public int DeleteUser(int userId)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create("http://localhost:56044/WcfDataService1.svc/" + "DeleteUser");
            using (Stream bodyStream = request.GetRequestStream())
            using (StreamWriter writer = new StreamWriter(bodyStream))
            {
                writer.Write("contractId: {0}", userId);
            }

            request.GetResponse();

            return 1;
        }*/
    }
}
