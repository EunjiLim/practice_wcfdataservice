﻿using System;
using System.Collections.Generic;
using BoardService2;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTestProject1
{
    [TestClass]
    public class NotificationServiceTest
    {
        private NotificationManagement _management;

        [TestInitialize]
        public void TestInitialize()
        {
            var testNotificationSet = new Dictionary<string, List<Notification>>();
            testNotificationSet.Add("Daeyeon", new List<Notification>() { new Notification() { Message = "Hi", NotifiedDate = DateTime.Now} });

            _management = new NotificationManagement(testNotificationSet);
        }

        [TestMethod]
        public void TestMethod1()
        {
            var noticications = _management.ListNotifications("Daeyeon");
            Assert.AreEqual(noticications.Count, 1);
        }
    }

    [TestClass]
    public class BoardServiceTest
    {
        private BoardManagement _management;

        [TestInitialize]
        public void TestInitialize()
        {
            var testBoardSet = new List<Article>();
            testBoardSet.Add(new Article(0, "SamKim", "first") {Content = "first SamKim's Article."});

            _management = new BoardManagement(testBoardSet);
        }

        [TestMethod]
        public void TestListArticles()
        {
            //arrange
            int id = 0;
            string author = "SamKim";
            string title = "first";
            string content = "first SamKim's Article.";

            //act
            var articleList = _management.ListArticles();

            //assert
            Assert.AreEqual(id, articleList[0].Id);
            Assert.AreEqual(author, articleList[0].Author);
            Assert.AreEqual(title, articleList[0].Title);
            Assert.AreEqual(content, articleList[0].Content);
        }

        [TestMethod]
        public void TestRead()
        {
            //arrange
            int id = 1;
            string author = "SamKim";
            string title = "first";
            string content = "first SamKim's Article.";

            //act
            var article = _management.Read(id);

            //assert
            Assert.AreEqual(id-1, article.Id);
            Assert.AreEqual(author, article.Author);
            Assert.AreEqual(title, article.Title);
            Assert.AreEqual(content, article.Content);
        }

        [TestMethod]
        public void TestWrite()
        {
            //arrange
            int id = 1;
            string author = "Monika";
            string title = "second";
            string content = "second Monika's Article.";
            
            //act
            var article = _management.Write(author, title, content);

            //assert
            Assert.AreEqual(id, article.Id);
            Assert.AreEqual(author, article.Author);
            Assert.AreEqual(title, article.Title);
            Assert.AreEqual(content, article.Content);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [TestClass]
    public class GettingEmptyCommentListTest
    {
        private Article _article;

        [TestInitialize]
        public void TestInitialize()
        {
            _article = new Article(0, "SamKim", "first SamKim's Article.");
        }

        [TestMethod]
        public void TestGettingEmptyCommentList()
        {
            //act
            var commentList = _article.GetCommentList();

            //assert
            Assert.AreEqual(null, commentList);
         }
    }

    [TestClass]
    public class GettingCommentListTest
    {
        private Article _article;
        private const int ExpectedCount = 1;
        private const int ExpectedId = 0;
        private const string ExpectedAuthor = "Solar";
        private const string ExpectedContent = "hello, I'm Solar.";

        [TestInitialize]
        public void TestInitialize()
        {
            _article = new Article(0, "SamKim", "first SamKim's Article.");
            _article.Comments = new List<Comment>()
            {
                new Comment(ExpectedId, ExpectedAuthor, ExpectedContent, _article)
            };
        }

        [TestMethod]
        public void TestGettingCommentList()
        {
            //arrange
            var expectedType = typeof (List<Comment>);
            
            //act
            var commentList = _article.GetCommentList();
            //assert
            Assert.AreEqual(expectedType, commentList.GetType());
            Assert.AreEqual(ExpectedCount, commentList.Count);
            Assert.AreEqual(ExpectedId, commentList[0].Id);
            Assert.AreEqual(ExpectedAuthor, commentList[0].Author);
            Assert.AreEqual(ExpectedContent, commentList[0].Content);
        }
    }

    [TestClass]
    public class WritingFirstCommentTest
    {
        private Article _article;
        private const int ExpectedCount = 1;
        private const int ExpectedId = 0;
        private const string ExpectedAuthor = "Solar";
        private const string ExpectedContent = "hello, I'm Solar.";

        [TestInitialize]
        public void TestInitialize()
        {
            _article = new Article(0, "SamKim", "first SamKim's Article.");
        }

        [TestMethod]
        public void TestWritingFirstComment()
        {
            //arrange

            //act
            _article.WriteComment(ExpectedAuthor, ExpectedContent);

            //assert
            Assert.AreEqual(ExpectedCount, _article.Comments.Count);
            //Assert.AreEqual(ExpectedId, _article.Comments[0].Id);
            //Assert.AreEqual(ExpectedAuthor, _article.Comments[0].Author);
            //Assert.AreEqual(ExpectedContent, _article.Comments[0].Content);
        }
        
    }

    [TestClass]
    public class WritingCommentTest
    {
        private Article _article;
        private const int ExpectedCount = 2;
        private const int CommentId = 0;
        private const string CommentAuthor = "Solar";
        private const string CommentContent = "hello, I'm Solar.";
        private const int ExpectedId = 1;
        private const string ExpectedAuthor = "Mamamu";
        private const string ExpectedContent = "Hello, I'm Mamamu.";

        [TestInitialize]
        public void TestInitialize()
        {
            _article = new Article(0, "SamKim", "first SamKim's Article.");
            _article.Comments = new List<Comment>()
            {
                new Comment(CommentId, CommentAuthor, CommentContent, _article),
            };
        }

        [TestMethod]
        public void TestWritingComment()
        {
            //arrange

            //act
            _article.WriteComment(ExpectedAuthor, ExpectedContent);
            
            //assert
            Assert.AreEqual(ExpectedCount, _article.Comments.Count);
            Assert.AreEqual(ExpectedId, _article.Comments[1].Id);
            Assert.AreEqual(ExpectedAuthor, _article.Comments[1].Author);
            Assert.AreEqual(ExpectedContent, _article.Comments[1].Content);
        }
    }
    //Notification 추가되는 부분 test해야 함~!!!!!!!! 어려울 것 같은데
}
