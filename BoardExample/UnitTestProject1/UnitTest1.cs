﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using BoardService;

namespace UnitTestProject1
{
    [TestClass]
    public class ArticleTests
    {
        private BoardWcfService _bws;

        [TestInitialize()]
        public void TestInitialize()
        {
            _bws = new BoardWcfService();
            _bws.Write("First Article", "EunjiKim", "This is first article.");
            _bws.Write("Second Article", "EunjiLim", "This is second article.");
            _bws.Write("Third Article", "EunjiSim", "This is third article.");
            _bws.Write("Forth Article", "YDKim", "This is forth article.");
        }

        [TestCleanup]
        public void TestCleanUp()
        {
            _bws.Clean();
        }

        [TestMethod]
        public void TestWrite()
        {
            //arrange
            string title = "Fifth Article";
            string author = "EunjiLim";
            string content = "Hello, this is Fifth article.";
            int expected = 5;

            //act  --> !매개변수가 null인 경우에도 
            int actual = _bws.Write(title, author, content);

            //assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void TestRead()
        {
            //arrange
            int id = 1;
            int expectedId = 0;
            string expectedAuthor = "EunjiKim";
            string expectedContent = "This is first article.";

            //act
            var actual = _bws.Read(id);

            //assert
            Assert.AreEqual(typeof(Article), actual.GetType());
            Assert.AreEqual(expectedId, actual.Id);
            Assert.AreEqual(expectedAuthor, actual.Author);
            Assert.AreEqual(expectedContent, actual.Content);
        }

        [TestMethod]
        public void TestListArticles()
        {
            //arrange
            int expectedCount = 4;
            string[] expectedTitle = { "First Article", "Second Article", "Third Article", "Forth Article" };

            //act
            var actual = _bws.ListArticles();
            
            //assert
            Assert.AreEqual(expectedCount, actual.Count);
            for (int i = 0; i < 4; i++)
            {
                Assert.AreEqual(expectedTitle[i], actual[i].Title);
            }
        }
    }

    [TestClass]
    public class CommentTests
    {
        BoardWcfService bws = new BoardWcfService();

        
    }
}
