﻿using System;
using System.Collections.Generic;
using System.Data.Services.Client;
using System.Diagnostics;
using System.Linq;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading.Tasks;
using ConsoleApplicationForTokenBasedSecurity.ServiceReference1;

namespace ConsoleApplicationForTokenBasedSecurity
{
    class Program
    {
        static void Main(string[] args)
        {
            InvokeODataSeprviceOeration();
        }

        static void InvokeODataSeprviceOeration()
        {
            var svcUri = new Uri("http://localhost:57201/WcfDataService1.svc");
            var ctx = new Board2Entities(svcUri);

            ctx.SendingRequest2 += (sender, args) =>
            {
                args.RequestMessage.SetHeader("auth-token", "admin");
            };

            try
            {
                var users = ctx.Users.ToList();
                Console.WriteLine("There are {0} users.", users.Count);
                Console.Read();
            }
            catch (DataServiceQueryException e)
            {
                Debug.WriteLine(e.ToString());
            }
     
        }
    }
}
