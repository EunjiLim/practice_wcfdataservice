﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;

namespace WcfDataService1IntegrationTest
{
    class DBConnection
    {
        private MySqlConnection con;
     
        public DBConnection(string connectionString)
        {
            // create con with connectionString
            con = new MySqlConnection(connectionString);
        }

        public void ClearTables()
        {
            ClearTable("Comments");
            ClearTable("Articles");
            ClearTable("Users");
            ClearTable("Notifications");
        }

        public void ClearTable(string name)
        {
            var cmd = con.CreateCommand();
            cmd.CommandType = CommandType.Text;
            string query = "delete from " + name;
            cmd.CommandText = query;
            int res = 0;
            try
            {
                con.Open();
                res = cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                cmd.Dispose();
                cmd = null;
                con.Close();
            }
        }

        public void InsertUsersForTest()
        {
            var cmd = con.CreateCommand();
            cmd.CommandType = CommandType.Text;
            string query = "insert into Users (name) values ('Yuna'), ('Hyori'), ('Uyu');";
            cmd.CommandText = query;
            try
            {
                con.Open();
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                cmd.Dispose();
                con.Close();
            }
        }

        public DataTable ExecuteSelectCommand(string CommandName, CommandType cmdType)
        {
     
            var table = new DataTable();

            var cmd = con.CreateCommand();

            cmd.CommandType = cmdType;
            cmd.CommandText = CommandName;

            try
            {
                con.Open();

                MySqlDataAdapter da = null;
                using (da = new MySqlDataAdapter(cmd))
                {
                    da.Fill(table);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                cmd.Dispose();
                cmd = null;
                con.Close();
            }

            return table;
        }

        public DataTable ExecuteParamerizedSelectCommand(string CommandName,
                         CommandType cmdType, SqlParameter[] param)
        { 
            DataTable table = new DataTable();

            var cmd = con.CreateCommand();

            cmd.CommandType = cmdType;
            cmd.CommandText = CommandName;
            cmd.Parameters.AddRange(param);

            try
            {
                con.Open();

                MySqlDataAdapter da = null;
                using (da = new MySqlDataAdapter(cmd))
                {
                    da.Fill(table);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                cmd.Dispose();
                cmd = null;
                con.Close();
            }

            return table;
        }

        //insert, delete, update
        public void ExecuteNonQuery(string CommandName, CommandType cmdType, MySqlParameter[] pars)
        {
            MySqlCommand cmd = null;
            int res = 0;
            string id = null;

            cmd = con.CreateCommand();

            cmd.CommandType = cmdType;
            cmd.CommandText = CommandName;
            cmd.Parameters.AddRange(pars);
           // cmd.Parameters.Add("@id", MySqlDbType.Int32).Direction = ParameterDirection.Output;
            
            try
            {
                con.Open();
                res = cmd.ExecuteNonQuery();
                // id = cmd.Parameters["@id"].Value.ToString();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                cmd.Dispose();
                cmd = null;
                con.Close();
            }
            // return Int32.Parse(id);
        }
        
        public void Close()
        {
            con.Close();
        }
    }
}
