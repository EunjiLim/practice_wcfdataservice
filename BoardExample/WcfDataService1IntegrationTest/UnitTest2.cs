﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Security.Policy;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MySql.Data.MySqlClient;
using WcfDataService1IntegrationTest.WcfDataService1;

namespace WcfDataService1IntegrationTest
{
    /// <summary>
    /// Summary description for UnitTest2
    /// </summary>
    [TestClass]
    public class UnitTest2
    {
        private DBConnection connection;
        private Board2Entities _boardService;
        

        [TestInitialize]
        public void Initialize()
        {
            connection = new DBConnection("server=45.63.124.63;user id=root;password=1234;port=5249;database=Board2;");
            connection.ClearTables();
            _boardService = new Board2Entities(new Uri("http://localhost:56044/WcfDataService1.svc"));

            string insertUsersQuery = "insert into Users (name) values (@user);";
            MySqlParameter[] userParameter1 = new[] {new MySqlParameter("user", "User1"), };
            MySqlParameter[] userParameter2 = new[] {new MySqlParameter("user", "User2"),};
            MySqlParameter[] userParameter3 = new[] { new MySqlParameter("user", "User3"), };
            connection.ExecuteNonQuery(insertUsersQuery, CommandType.Text, userParameter1);
            connection.ExecuteNonQuery(insertUsersQuery, CommandType.Text, userParameter2);
            connection.ExecuteNonQuery(insertUsersQuery, CommandType.Text, userParameter3);

            string selectUserQuery = "select id from Users where name='User1'";
            var idTable = connection.ExecuteSelectCommand(selectUserQuery, CommandType.Text);
            var stringId = idTable.Rows[0]["id"].ToString();
            var _user1Id = Int32.Parse(stringId);

            string insertArticlesQuery = "insert into Articles (user_id, title, content) " +
                                         "values (@user, @title, @content);";
            var articleParameters = new[]
            {
                new MySqlParameter("user", _user1Id),
                new MySqlParameter("title", "Title1"),
                new MySqlParameter("content", "Content1"),
            };
            connection.ExecuteNonQuery(insertArticlesQuery, CommandType.Text, articleParameters);

            string selectUserQuery2 = "select id from Users where name='User2'";
            var idTable2 = connection.ExecuteSelectCommand(selectUserQuery2, CommandType.Text);
            var stringId2 = idTable2.Rows[0]["id"].ToString();
            var _user2Id = Int32.Parse(stringId2);

            string selectArticleQuery = "select id from Articles where user_id=" + _user1Id;
            var idTable3 = connection.ExecuteSelectCommand(selectArticleQuery, CommandType.Text);
            var stringId3 = idTable3.Rows[0]["id"].ToString();
            var _user3Id = Int32.Parse(stringId3);

            string insertCommentsQuery = "insert into Comments (user_id, article_id, content) " +
                                         "values (@user, @article, @content)";
            MySqlParameter[] commentParameters = new[]
            {
                new MySqlParameter("user", _user2Id),
                new MySqlParameter("article", _user3Id), 
                new MySqlParameter("content", "Comment1")
            };

            // cnn.ClearTable()
            // for () conn.Insert(..)
        }

        [TestMethod]
        public void TestAddUser()
        {
            var expectedName = "Ashley";

            var users = new Users()
            {
                name = expectedName
            };

            _boardService.AddToUsers(users);
            _boardService.SaveChanges();

            var selectedUser = (from u in _boardService.Users
                                 where u.name == expectedName
                                 select u).First();

            Assert.AreEqual(expectedName, selectedUser.name);
        }

        [TestMethod]
        public void TestAddComment()
        {
            string selectUserQuery = "select id from Users where name='User3'";
            var idTable = connection.ExecuteSelectCommand(selectUserQuery, CommandType.Text);
            var stringId = idTable.Rows[0]["id"].ToString();
            var id = Int32.Parse(stringId);

            string selectUserQuery2 = "select id from Users where name='User1'";
            var idTable2 = connection.ExecuteSelectCommand(selectUserQuery2, CommandType.Text);
            var stringId2 = idTable2.Rows[0]["id"].ToString();
            var id2 = Int32.Parse(stringId2);

            string selectArticleQuery = "select id from Articles where user_id=" + id2;
            var idTable3 = connection.ExecuteSelectCommand(selectArticleQuery, CommandType.Text);
            var stringId3 = idTable3.Rows[0]["id"].ToString();
            var id3 = Int32.Parse(stringId3);

            var comment = new Comments()
            {
                user_id = id,
                article_id = id3,
                content = "hahahahahaha after lunch uuu"
            };

            //assert
            Assert.AreEqual(2, _boardService.Notifications.Count());
        }

        [TestCleanup]
        public void ClearConnections()
        {
            connection.Close();
        }
    }
}
