﻿using BoardService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace BoardHost
{
    class HostApp
    {
        static void Main(string[] args)
        {
            ServiceHost host = new ServiceHost(typeof(BoardWcfService),
                new Uri("http://172.20.10.12/wcf/example/boardservice"));

            host.AddServiceEndpoint(
                typeof(IArticleManagement),        // service contract
                new BasicHttpBinding(),      // service binding
                "");

            host.Open();
            Console.WriteLine("Press Any key to stop the service");
            Console.ReadKey(true);
            host.Close();
        }
    }
}
