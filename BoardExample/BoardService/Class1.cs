﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace BoardService
{
    // 서비스 Contract 선언
    [ServiceContract]
    public interface IArticleManagement
    {
        [OperationContract]
        int AttachComment(int id, string author, string content);

        [OperationContract]
        List<Article> ListArticles();

        [OperationContract]
        List<Comment> ListComments(int id);

        [OperationContract]
        Article Read(int id);

        [OperationContract]
        int Write(string title, string author, string content);

        [OperationContract]
        List<Notification> ListNotification(string author);

        [OperationContract]
        void Clean();
    }

    // 서비스 타입 구현
    public class BoardWcfService : IArticleManagement
    {
        private static List<Article> _articles = new List<Article>();

        private static Dictionary<string, List<Notification>> _notificationDict = new Dictionary<string, List<Notification>>();

        public int AttachComment(int id, string author, string content)
        {
            //1. comment를 생성한다.
            var comment = Comment.Create(_articles[id - 1], author, content);

            //2. NotifyToRelatedAuthors(어떤 article, 어떤 notification)
            NotifyToRelatedAuthors(_articles[id - 1], Notification.CreateForCreatingComment(comment, DateTime.Now));

            //3. article에 1번에서 생성한 comment를 붙힌다.
            _articles[id - 1].AddComment(comment);

            return comment.Id;
        }

        private void NotifyToRelatedAuthors(Article article, Notification notification)
        {
            NotifyToAuthor(article.Author, notification);
            foreach (var c in article.Comments)
                NotifyToAuthor(c.Author, notification);
        }

        private void NotifyToAuthor(string author, Notification notification)
        {
            var notifications = _notificationDict[author] ?? new List<Notification>();
            notifications.Add(notification);
            if (!_notificationDict.ContainsKey(author))
                _notificationDict[author] = notifications;
        }

        public List<Article> ListArticles()
        {
            return _articles;
        }
        public List<Comment> ListComments(int id)
        {
            return _articles[id-1].Comments;
        }
        public Article Read(int id)
        {
            return _articles[id-1];
        }
        public int Write(string title, string author, string content)
        {
            _articles.Add(new Article()
            {
                Id = _articles.Count(),
                Title = title,
                Author = author,
                Content = content
            });
            return _articles.Count;
        }
        public List<Notification> ListNotification(string author)
        {
            return _notificationDict[author];
        }
        public void Clean()
        {
            _articles.Clear();
            _notificationDict.Clear();
        }
    }

    [DataContract]
    public class Article
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public string Title { get; set; }

        [DataMember]
        public string Author { get; set; }

        [DataMember]
        public string Content { get; set; }

        [DataMember]
        public List<Comment> Comments { get; set; }

        /*public Article(int id, string title, string author)
        {
            Id = id;
            Title = title;
            Author = author;
        }*/

        [DataContractFormat]
        public void AddComment(Comment comment)
        {
            Comments.Add(comment);
        }
    }

    [DataContract]
    public class Comment
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public string Author { get; set; }

        [DataMember]
        public string Content { get; set; }

        [DataMember]
        public Article Article { get; set; }

        public static Comment Create(Article article, string author, string content)
        {
            return new Comment
            {
                Id = article.Comments.Count,
                Author = author,
                Content = content,
                Article = article
            };
        }
    }
   
    [DataContract]
    public class Notification
    {
        [DataMember]
        public string Message { get; set; }

        [DataMember]
        public DateTime NotifiedDate { get; set; }

        public static Notification CreateForCreatingComment(Comment comment, DateTime now)
        {
            return new Notification
            {
                Message = comment.Author + " writed a comment on Article " + comment.Article.Id,
                NotifiedDate = now
            };
        }
    }
}
