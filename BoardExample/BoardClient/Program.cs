﻿using BoardService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Description;
using System.Text;
using System.Threading.Tasks;

namespace BoardClient
{
    class Program
    {
        static void Main(string[] args)
        {
            Uri uri =
                    new Uri("http://localhost/wcf/example/boardservice");
            ServiceEndpoint ep = new ServiceEndpoint(
                    ContractDescription.GetContract(typeof(IArticleManagement)),
                    new BasicHttpBinding(),
                    new EndpointAddress(uri));

            ChannelFactory<IArticleManagement> factory =
                    new ChannelFactory<IArticleManagement>(ep);
            IArticleManagement proxy = factory.CreateChannel();
            int result = proxy.Write("first article", "eunjilim", "This is First Article");
            (proxy as IDisposable).Dispose();

            Console.WriteLine(result);
        }
    }
}
