//------------------------------------------------------------------------------
// <copyright file="WebDataService.svc.cs" company="Microsoft">
//     Copyright (c) Microsoft Corporation.  All rights reserved.
// </copyright>
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Data.Services;
using System.Data.Services.Common;
using System.Linq;
using System.Linq.Expressions;
using System.Security.Principal;
using System.ServiceModel.Web;
using System.Web;

namespace WebApplicationForRoleBasedSecurity
{
    public class WCFBoardService : DataService<Board2Entities>
    {
        // This method is called only once to initialize service-wide policies.
        public static void InitializeService(DataServiceConfiguration config)
        {
            // TODO: set rules to indicate which entity sets and service operations are visible, updatable, etc.
            // Examples:
            // config.SetEntitySetAccessRule("MyEntityset", EntitySetRights.AllRead);
            // config.SetServiceOperationAccessRule("MyServiceOperation", ServiceOperationRights.All);
            config.SetEntitySetAccessRule("Users", EntitySetRights.All);
            config.SetEntitySetAccessRule("Articles",EntitySetRights.All);
            config.DataServiceBehavior.MaxProtocolVersion = DataServiceProtocolVersion.V3;
        }

        [QueryInterceptor("User")]
        public Expression<Func<User, bool>> OnQueryUsers()
        {
            var user = HttpContext.Current.User;
            if (user.IsInRole("Administrator"))
                return (c) => true;
            else
                throw new DataServiceException(401, "Unauthorized User!");
        }

        [QueryInterceptor("Article")]
        public Expression<Func<Articles, bool>> OnQueryArticles()
        {
            var user = HttpContext.Current.User;
            if (user.IsInRole("User"))
                return (p) => true;
            else
            {
                throw new DataServiceException(401, "Unauthorized User!");
            }
        }
    }

    //서버 실행 시 request오면 자동으로.. 
    public class RoleBasedAuthModule : IHttpModule
    {
        public void Init(HttpApplication context)
        {
            context.AuthenticateRequest += OnAuthenticateRequest;
        }

        public void Dispose()
        {
        }

        void OnAuthenticateRequest(object sender, EventArgs e)
        {
            try
            {
                var app = sender as HttpApplication;
                var request = app?.Context.Request;
                var userId = request?.Headers["odata-user-id"];

                if (string.IsNullOrEmpty(userId))
                    userId = "Anonymous";

                var roles = GetRolesForUser(userId);

                var gPrincipal = new GenericPrincipal(
                    new GenericIdentity(userId),
                    roles);

                app.Context.User = gPrincipal;
               
            }
            catch (Exception ee)
            {
                ee.ToString();
            }
        }

        #region -- Helper Methods --
        string[] GetRolesForUser(string userId)
        {
            if (userId == "Admin")
                return new [] {"Administrator", "User"};

            if (userId == "User")
                return new [] {"User"};

            return new [] {""};
        }
        #endregion
    }
}