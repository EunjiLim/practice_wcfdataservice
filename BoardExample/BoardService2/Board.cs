﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace BoardService2
{
    internal class BoardStorage     //storage
    {
        public static readonly List<Article> ArticleList = new List<Article>();
    }

    [ServiceContract]
    public interface IBoardManagement
    {
        [OperationContract]
        IList<Article> ListArticles();

        [OperationContract]
        Article Read(int id);

        [OperationContract]
        Article Write(string title, string author, string content);
    }

    public class BoardManagement : IBoardManagement       //controller
    {
        public IList<Article> ArticleList { get; set; }

        public BoardManagement(IList<Article> articleList)
        {
            ArticleList = articleList;
        }

        public IList<Article> ListArticles()
        {
            return ArticleList;
        } 

        public Article Read(int id)
        {
            return ArticleList[id - 1];
        }

        public Article Write(string author, string title, string content)
        {
            Article article = new Article(ArticleList.Count, author, title)
            {
                Content = content
            };
            ArticleList.Add(article);
            return article;
        }
    }

    public class BoardWcfService : IBoardManagement      //boundary
    {
        private BoardManagement management = new BoardManagement(BoardStorage.ArticleList);

        //private static List<Article> Articles = new List<Article>();

        public IList<Article> ListArticles()
        {
            return management.ListArticles();
        }

        public Article Read(int id)
        {
            return management.Read(id);
        }

        public Article Write(string author, string title, string content)
        {
            return management.Write(author, title, content);
        }
    }
    // --------------------------------------------------------------------------
    // --------------------------------------------------------------------------

    // Singleton
    internal class NotificationStorage
    {
        public static readonly Dictionary<string, List<Notification>> NotificationDict = new Dictionary<string, List<Notification>>();
    }

    [ServiceContract]
    public interface INotificationManagement
    {
        [OperationContract]
        List<Notification> ListNotifications(string author);
    }

    public class NotificationManagement : INotificationManagement
    {
        public IDictionary<string, List<Notification>> NotificationDict { get; set; }

        public NotificationManagement(IDictionary<string, List<Notification>> notificationDict)
        {
            NotificationDict = notificationDict;
        }

        public List<Notification> ListNotifications(string author)
        {
            return NotificationDict[author];
        }
    }

    public class NotificationWcfService : INotificationManagement    
    {
        private NotificationManagement management = new NotificationManagement(NotificationStorage.NotificationDict);
        //private static Dictionary<string, List<Notification>> NotificationDict =

        public List<Notification> ListNotifications(string author)
        {
           return management.ListNotifications(author);
        }
    }

 

    public class Notification
    {
        [DataMember]
        public string Message { get; set; }

        [DataMember]
        public DateTime NotifiedDate { get; set; }

        public static Notification CreateForCreatingComment(Comment comment, DateTime notifiedDate)
        {
            return new Notification
            {
                Message = comment.Author+ "wrote a comment on Article" + comment.Article.Id,
                NotifiedDate = notifiedDate
            };
        }
    }

    public class Article
    {
        [DataMember]
        public string Author { get; set; }

        [DataMember]
        public List<Comment> Comments { get; set; }

        [DataMember]
        public string Content { get; set; }

        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public string Title { get; set; }

        public Article(int id, string author, string title)
        {
            Id = id;
            Author = author;
            Title = title;
        }

        public List<Comment> GetCommentList()
        {
            return Comments;
        }

        public void WriteComment(string author, string content)
        {
            var comment = new Comment(Comments.Count, author, content, this);
            NotifyToRelatedAuthors(this, Notification.CreateForCreatingComment(comment, DateTime.Now));
            Comments.Add(comment);
        }

        private void NotifyToRelatedAuthors(Article article, Notification notification)
        {
            NotifyToAuthor(article.Author, notification);

            foreach (var c in article.Comments)
                NotifyToAuthor(c.Author, notification);
        }

        private void NotifyToAuthor(string author, Notification notification)
        {
            var notifications = NotificationStorage.NotificationDict[author] ?? new List<Notification>();
            notifications.Add(notification);
            if (!NotificationStorage.NotificationDict.ContainsKey(author))
                NotificationStorage.NotificationDict[author] = notifications;
        }
    }

    public class Comment
    {
        [DataMember]
        public string Author { get; set; }

        [DataMember]
        public string Content { get; set; }

        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public Article Article { get; set; }

        public Comment(int id, string author, string content, Article article)
        {
            Id = id;
            Author = author;
            Content = content;
            Article = article;
        }
    }
}
